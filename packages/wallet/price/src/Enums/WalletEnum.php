<?php

namespace Wallet\Price\Enums;

class WalletEnum
{
    const USD = 1;
    const LABEL_USD = "USD";

    const GBP = 2;
    const LABEL_GBP = "GBP";

    const HKD = 3;
    const LABEL_HKD = "HKD";

    const BITCOIN = 4;
    const LABEL_BITCOIN = "BTC";

    const ETHEREUM = 5;
    const LABEL_ETHEREUM = "ETH";

    const TETHER = 6;
    const LABEL_TETHER = "USDT";

    const LIST_TYPE_PRICE = array(
        array(
            'label' => self::LABEL_USD,
            'value' => self::USD,
        ),
        array(
            'label' => self::LABEL_GBP,
            'value' => self::GBP,
        ),
        array(
            'label' => self::LABEL_HKD,
            'value' => self::HKD,
        ),
        array(
            'label' => self::LABEL_BITCOIN,
            'value' => self::BITCOIN,
        ),
        array(
            'label' => self::LABEL_ETHEREUM,
            'value' => self::ETHEREUM,
        ),
        array(
            'label' => self::LABEL_TETHER,
            'value' => self::TETHER,
        )
    );

}
