<?php

namespace Wallet\Price\Providers;

use Illuminate\Support\ServiceProvider;

class PriceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

    }
}
