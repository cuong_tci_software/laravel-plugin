<?php

namespace Wallet\Price;
use Wallet\Price\Enums\WalletEnum;

use Illuminate\Support\Facades\Http;

class Price {
    public function sum($amount, $price) {
        return $amount + $price;
    }

    public function sub($amount, $price) {
        return $amount - $price;
    }

    public function listTypePrice() {
        return WalletEnum::LIST_TYPE_PRICE;
    }
}
