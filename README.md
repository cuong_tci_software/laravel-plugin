# Install project

Docker-compose up -d <br>
docker-compose exec db bash <br>
mysql -u root -p your_mysql_root_password <br>
GRANT ALL ON laravel.* TO 'laraveluser'@'%' IDENTIFIED BY 'password' <br>
docker-compose exec app bash <br>
composer install <br>
npm install && npm run dev <br>
php artisan migrate <br>
localhost/wallet <br>
