<!DOCTYPE html>
<html lang="en">
<head>
  <title>Wallet</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>
<div class="container mt-2">
      <form method="post" id="calculate-form">
            @csrf
            <div class="form-group">
                    <label for="price">Price:</label>
                    <input required type="number" name="price" class="form-control" placeholder="Enter price" id="price">
                  </div>

                  <div class="form-group">
                        <label>Calculation</label>
                        <select class="form-control" name="calculation" id="calculation">
                                <option value="+">Addition</option>
                                <option value="-">Subtraction</option>
                        </select>
                  </div>

                  <div class="form-group">
                        <label>Type price:</label>
                        <select class="form-control" name="type" id="type">
                                @foreach($listTypePrice as $key => $val)
                                    <option value={{$val['value']}}>{{  $val['label']   }}</option>
                                @endforeach
                        </select>
                  </div>


                  <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      <div id="result-calculate"></div>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
$(document).ready(function () {

$("#calculate-form").submit(function (e) {
    e.preventDefault();
    let formData = {
      price: $("#price").val(),
      calculation: $("#calculation").val(),
      type: $("#type").val(),
      '_token':'{{ csrf_token() }}'
    };
    $.ajax({
    url: '{{ route('calculate.price') }}',
    type: 'post',
    data: formData,
    success: function(response)
    {
       let message = response.message.replace(":price", response.price).replace(":total", formatPrice(response.total));
       $("#result-calculate").html(message + ' ' + $( "#type option:selected" ).text());
    },
    error: function (error) {
        $("#result-calculate").html(JSON.parse(error.responseText).message);
    },
});

    return true;
});
});
function formatPrice(nStr){
  nStr += '';
  x = nStr.split('.');
  x1 = x[0];
  x2 = x.length > 1 ? '.' + x[1] : '';
  var rgx = /(\d+)(\d{3})/;
  while (rgx.test(x1)) {
    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  }
  return x1 + x2;
}
</script>

</body>
</html>
