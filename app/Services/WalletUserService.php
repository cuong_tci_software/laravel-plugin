<?php

namespace App\Services;

use App\Models\WalletUser;

class WalletUserService
{
    protected $walletUser;

    public function __construct(WalletUser $walletUser)
    {
        $this->walletUser = $walletUser;
    }

    public function find($attr)
    {
        return $this->walletUser->where([
            ['user_id' , $attr['user_id']],
            ['type' , $attr['type']]
        ])->first();
    }

    public function create($attr)
    {
        return $this->walletUser->create($attr);
    }

    public function update($attr)
    {
        return $this->find($attr)->update($attr);
    }
}
