<?php

namespace App\Services;

use App\Models\LockTransaction;

class LockTransactionService
{
    protected $lockTransaction;

    public function __construct(LockTransaction $lockTransaction)
    {
        $this->lockTransaction = $lockTransaction;
    }

    public function create($attr)
    {
        return $this->lockTransaction->create($attr);
    }
}
