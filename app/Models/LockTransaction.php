<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LockTransaction extends Model
{

    protected $fillable = [
        'user_id',
        'price',
        'type',
        'calculation'
    ];
}
