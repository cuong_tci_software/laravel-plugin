<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Wallet\Price\Price;
use DB;
use App\Enums\PriceEnum;
use App\Models\WalletUser;
use App\Services\WalletUserService;
use App\Services\LockTransactionService;

class InspirationController extends Controller
{
    protected $walletUserService;
    protected $lockTransactionService;

    public function __construct(WalletUserService $walletUserService, LockTransactionService $lockTransactionService)
    {
        $this->walletUserService = $walletUserService;
        $this->lockTransactionService = $lockTransactionService;
    }

    public function index(Price $price)
    {
        $listTypePrice = $price->listTypePrice();
        return view("calculate", compact("listTypePrice"));
    }

    public function calculate(Request $req, Price $wallet)
    {
        DB::beginTransaction();

        try {
            $user = Auth::user();
            $result = 0;
            $price = $req->input("price");
            $attr = [
                "type" => $req->input("type"),
                "user_id" => $user->id,
            ];

            $walletUser = $this->walletUserService->find($attr);

            if ($walletUser) {
                $attr["amount"] = $walletUser->amount;
            } else {
                $attr["amount"] = 0;
            }

            if ($req->input("calculation") == PriceEnum::ADDITION) {
                $result = $wallet->sum($attr["amount"], $price);
                $message =
                    "You just added :price into account. Your current account has :total";
                $attr['calculation'] = PriceEnum::ADDITION_VALUE;
            } else {
                $message =
                    "You just subtracted :price in accounts. Your current account has :total";
                $result = $wallet->sub($attr["amount"], $price);
                $attr['calculation'] = PriceEnum::SUBTRACTION_VALUE;
            }

            $attr["amount"] = $result;

            if ($result < 0) {
                return response()->json(
                    [
                        "success" => false,
                        "message" => "There is not enough money in the account to make the transaction. Please check again",
                    ],
                    400
                );
            }

            if (!$walletUser) {
                $this->walletUserService->create($attr);
            } else {
                $this->walletUserService->update($attr);
            }

            $attr['price'] = $price;
            $this->lockTransactionService->create($attr);
            DB::commit();

            return response()->json([
                "success" => true,
                "message" => $message,
                "price" => $price,
                "total" => $result,
            ]);

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
        }
    }
}
