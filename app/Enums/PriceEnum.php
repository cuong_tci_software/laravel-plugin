<?php

namespace App\Enums;

class PriceEnum
{
    const ADDITION = "+";
    const SUBTRACTION = "-";
    const ADDITION_VALUE = 1;
    const SUBTRACTION_VALUE = 2;
}
